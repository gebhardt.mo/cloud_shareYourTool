import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { OrganisationComponent } from './organisation/organisation.component';
import { ToolComponent } from './tool/tool.component';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule} from '@angular/material/grid-list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ToolCreateComponent } from './tool-create/tool-create.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CreateOrganisationComponent } from './create-organisation/create-organisation.component';
import { FaqComponent } from './faq/faq.component';
import { FrontendDevelopmentComponent } from './frontend-development/frontend-development.component';
import { ContactComponent } from './contact/contact.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { OrganizationPageComponent } from './organization-page/organization-page.component';
import { SocialLoginModule,
         GoogleLoginProvider, 
         SocialAuthServiceConfig } from 'angularx-social-login';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { DialogContactInformationComponent } from './dialog-contact-information/dialog-contact-information.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OrganisationComponent,
    ToolComponent,
    ToolCreateComponent,
    LoginComponent,
    RegisterComponent,
    CreateOrganisationComponent,
    FaqComponent,
    FrontendDevelopmentComponent,
    ContactComponent,
    ImpressumComponent,
    OrganizationPageComponent,
    DialogContactInformationComponent
  ],
  entryComponents:[DialogContactInformationComponent],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatProgressBarModule,
    MatGridListModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule,
    SocialLoginModule,
    MatDialogModule,
    MatSnackBarModule,
    MatProgressSpinnerModule
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider('622847916367-p39k4nu4l19dg0qokghtb94s6lcthlc4.apps.googleusercontent.com'),
          },
          
        ],  
      }  as SocialAuthServiceConfig,
    }
    
  ],  
    
  bootstrap: [AppComponent]
})
export class AppModule { }

export class UserInfoModel
{
	guid: string;
	customerUid: string;
	
	first_name: string;
	last_name: string;

	email: string;
	zipcode: string;

	password: string;

	constructor(obj: any = null)
	{
		if(obj != null)
		{
			Object.assign(this, obj);
		}
	}
}