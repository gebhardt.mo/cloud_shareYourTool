import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-contact-information',
  templateUrl: './dialog-contact-information.component.html',
  styleUrls: ['./dialog-contact-information.component.css']
})
export class DialogContactInformationComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

}
