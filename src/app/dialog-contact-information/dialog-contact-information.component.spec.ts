import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogContactInformationComponent } from './dialog-contact-information.component';

describe('DialogContactInformationComponent', () => {
  let component: DialogContactInformationComponent;
  let fixture: ComponentFixture<DialogContactInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogContactInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogContactInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
