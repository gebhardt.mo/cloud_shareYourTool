
export interface organization {
    id: string;
    nameOrg: string;
    subtitle: string;
    image: string;
    members: string;
}