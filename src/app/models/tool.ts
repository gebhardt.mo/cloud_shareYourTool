export interface tool {
    id: string;
    user_id: string;
    image: string;
    designation: string;
    description: string;
    create: string;
}