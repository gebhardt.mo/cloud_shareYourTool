import { organization } from './organization';
export interface user {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    street: string;
    postCode: number;
    tokenId: string;    
    phone: number;
    organizations: organization[];
}