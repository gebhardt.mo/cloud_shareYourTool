import { Component, OnInit } from '@angular/core';
import { organization } from '../models/organization';
import { tool } from '../models/tool';
import { ActivatedRoute } from '@angular/router';
import { OrganizationService } from '../services/organization.service';
import { ToolService } from '../services/tool.service';
import { MatDialogModule, MatDialog} from '@angular/material/dialog';
import { DialogContactInformationComponent } from '../dialog-contact-information/dialog-contact-information.component';
import { UserService } from '../services/user.service';
import { user } from '../models/user';

@Component({
  selector: 'app-organization-page',
  templateUrl: './organization-page.component.html',
  styleUrls: ['./organization-page.component.css']
})
export class OrganizationPageComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
              private organizationService: OrganizationService,
              private toolService: ToolService,
              private userService: UserService,
              public dialog: MatDialog) { }
  
  organization: organization;
  user: user;
  tools: tool[];
  orgaID: string;
  title: string;
  description: string;
  image: string;

  ngOnInit(): void {
    
    /* Get URL Params for Organization ID */
    this.orgaID = this.activatedRoute.snapshot.paramMap.get('orgaID');

    this.organizationService.getOrganization(this.orgaID).subscribe(res => {
      this.description = res['Item'].subtitle;
      this.title = res['Item'].nameOrg;
      this.image = res['Item'].image;     
    });


    this.toolService.getAllToolsOfOrganization(this.orgaID).subscribe((tools: tool[]) => {
      this.tools = tools;
    });
}


  openDialog(user_id: string){

    this.userService.getUser(user_id).subscribe(res => {
      this.user = res['Item'];
      // console.log(this.user);
      this.showPersonInfo();
    });
  }

  showPersonInfo(){
    this.dialog.open(DialogContactInformationComponent, {data:{
      firstName: this.user.firstName, 
      lastName: this.user.lastName, 
      phone: this.user.phone, 
      street: this.user.street, 
      postCode: this.user.postCode, 
      email: this.user.email}});
  }
}