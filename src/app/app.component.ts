import { Component } from '@angular/core';
import { SocialAuthService } from "angularx-social-login";
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private authService: SocialAuthService, private router: Router) { }

  title = 'shareYourTool';

  signOut(): void {
    this.authService.signOut();
    //console.log('User was logged out.');
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }
}
