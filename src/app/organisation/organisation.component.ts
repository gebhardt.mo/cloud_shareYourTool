import { joinOrganization } from './../models/joinOrganization';
import { organization } from './../models/organization';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrganizationService } from '../services/organization.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { timer } from 'rxjs';

@Component({
  selector: 'app-organisation',
  templateUrl: './organisation.component.html',
  styleUrls: ['./organisation.component.css']
})
export class OrganisationComponent implements OnInit {

  constructor(private organizationService: OrganizationService,
    private snackbar: MatSnackBar,
    private router: Router) { }

  organizations: organization[];

  ngOnInit(): void {


    this.organizationService.getAllOrganizationNotJoined(localStorage.getItem('userID')).subscribe((organizations: organization[]) => {
      this.organizations = organizations;
      // console.log(this.organizations);
    });

  }

  joinOrganization(orgaID: string) {

    const joinOrganization: joinOrganization = {
      user_id: localStorage.getItem('userID'),
      organization_id: orgaID
    };

    this.organizationService.joinOrganization(joinOrganization).subscribe(() => {
      const source = timer(1000);
      source.subscribe(() => {
        this.router.navigateByUrl('/home');
        this.snackbar.open("Sie sind erfolgreich der Organisation beigetreten", "X", {
          duration: 10000,
          horizontalPosition: "end",
          verticalPosition: "top",
        });
      });

    });
  }

}

