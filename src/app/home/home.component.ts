import { OrganizationService } from './../services/organization.service';
import { ToolService } from './../services/tool.service';
import { Component, OnInit } from '@angular/core';
import { organization } from '../models/organization';
import { tool } from '../models/tool';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  organizations: organization[];
  tools: tool[];
  isLoadingOrga =  true;
  isLoadingTool = true;

  constructor(
    private organizationService: OrganizationService,
    private toolService: ToolService
  ) {}

  ngOnInit(): void {

      if (localStorage.getItem('userID')){

        const userID: string = localStorage.getItem('userID');

        // console.log(userID);
        this.organizationService.getAllOrganizationJoined(userID).subscribe((organizations: organization[]) => {
          this.organizations = organizations;
          this.isLoadingOrga = false;
          // console.log(this.organizations);
        });
        this.toolService.getAllToolsFromUser(userID).subscribe(res => {
          this.tools = res['Items'];
          this.isLoadingTool = false;
          // console.log(this.tools);
        });

      }

 }

}
