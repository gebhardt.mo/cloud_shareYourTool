import { Component, OnInit } from '@angular/core';
import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { UserService } from './../services/user.service';
import { Router } from '@angular/router';
import { user } from '../models/user';
import { organization } from '../models/organization';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: user;
  userString: string;
  userJSON: JSON;
  userId: string;

  constructor(private authService: SocialAuthService, private userService: UserService, private router: Router) { }


  signInWithGoogle() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((userData) => {


        this.userService.getUserByToken(userData.email).subscribe((userDynamo: user) => {
          // console.log(userDynamo[0])


          //userDynamo[0]['tokenId'] == null

          if (userDynamo[0]) {
            // console.log("User does exist");
            this.userId = userDynamo[0]['id'];
            localStorage.setItem('userID', userDynamo[0]['id']);
            this.router.navigateByUrl('/home');
          } else {
            // console.log("User does not exist");
            this.router.navigateByUrl('/registration', { state: { data: { token: userData.email } } })

          }

        });
      });
  }

  ngOnInit(): void {
  }

}
