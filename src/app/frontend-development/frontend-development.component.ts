import { Component, OnInit } from '@angular/core';
import {MatDialogModule, MatDialog} from '@angular/material/dialog';
import { DialogContactInformationComponent } from '../dialog-contact-information/dialog-contact-information.component';


@Component({
  selector: 'app-frontend-development',
  templateUrl: './frontend-development.component.html',
  styleUrls: ['./frontend-development.component.css']
})
export class FrontendDevelopmentComponent implements OnInit {
  
  
  constructor(public dialog: MatDialog) {  }

  openDialog(){
    this.dialog.open(DialogContactInformationComponent, {data:{firstName: 'Tassilo', lastName: 'Habig', phone: '+49745415', street: 'Alfred-Strasse 60', postCode: '76489 Konstanz', email: 'ta391hab@htwg-kn.de' }});
  }

  ngOnInit(): void {
  }
  
  
  orgaName: String;
  orgaDescription: String;
  orgaDescriptionLong: String;
  orgaPicture: File;

  createOrganisation(){
    /*Zuweisung der Werte aus dem Forumular funktioniert*/
    const orgaName = this.orgaName;
    const orgaDescription = this.orgaDescription;
    const orgaDescriptionLong = this.orgaDescriptionLong;
    const orgaPicture = this.orgaPicture;

    //console.log("function createOrganisation called");
    //console.log("Name: "+ orgaName + ", Beschreibung: "+ orgaDescription);

    
  }

 /*
  addDefaultData(){
    console.log("AddDefaultOrganisations called");
  
    this.organisations.push( { title: "HTWG Konsanz", description: "Hier können alle HTWG-Studis ihre Werkzeuge teilen. ", picture: { url: 'https://www.rnz.de/cms_media/module_img/580/290036_2_org_image_62e5f9b6249e3cf6.jpg' } });
    this.organisations.push( { title: "Universität Konstanz", description: "Hier können alle Studierende der Uni Konstanz ihre Werkzeuge teilen.", picture: { url: 'https://www.stuve.uni-konstanz.de/typo3temp/secure_downloads/107687/0/bb363740b16188c3c59371ad2dcc14fa14172ba8/csm_StuVe_Logo_188552d9da.png' } });
    this.organisations.push( { title: "Bürgerbüro Konstanz", description: "Auch das Bürgerbüro stellt verschiedene Dinge für Jedermann zur freien Verfügung.", picture: { url: 'https://www.konstanz.de/site/Konstanz/get/params_E486985068/77390/B%C3%BCrgerb%C3%BCro%20VGL_1640_690.jpg' } });
    this.organisations.push( { title: "Gartenfreunde Konstanz", description: "Garten-Tools des Vereins der Gartenfreude am Bismarckturm e.V. ", picture: { url: 'https://www.gartenfreunde-konstanz.de/blume10_448_336.jpg' } });
    this.organisations.push( { title: "Bürgergemeinschaft Petershausen e.V.", description: "Hole Tools ab im Herzen Petershausens", picture: { url:'https://www.bg-petershausen.de/medien/Bilder/2010-04-17_Rundgang_Petershausen_Ost_1/Petershauser%20Park.jpg'} });
  
    this.users.push( { surname: "Gebhardt", name: "Moritz"});
    this.users.push( { surname: "Hauber", name: "Daniel"});
    this.users.push( { surname: "Müller", name: "Pascal"});
    this.users.push( { surname: "Habig", name: "Tassilo"});

    this.tools.push( { toolName: "Hammer", toolDescription: "Zum Schlagen"});
    this.tools.push( { toolName: "Rasenmäher", toolDescription: "Teile meinen neuen NDDI-430-Y"});
    this.tools.push( { toolName: "Bohrmaschine", toolDescription: "Eine Bosch f-500 mit den dazugehörigen Bohrern"});
    this.tools.push( { toolName: "Akkuschrauber", toolDescription: "Einwandfreies Teil, 4.5 Stunden Ladezeit bei 3 Stunden Betriebszeit"});
  }

  orgaName = "HTWG Konsanz";
  orgaDescriptionLong = "Hier können alle HTWG-Studis ihre Werkzeuge teilen. Die HTWG ist die FH von Konstanz und hat über 5000 Studierende.";
  picture: { url:"https://www.bg-petershausen.de/medien/Bilder/2010-04-17_Rundgang_Petershausen_Ost_1/Petershauser%20Park.jpg"} ;
  */
  /*
    organisations: any[] =[];

  addDefaultOrganisations(){
    console.log("AddDefaultOrganisations called");
  
    this.organisations.push( { title: "HTWG Konsanz", description: "Hier können alle HTWG-Studis ihre Werkzeuge teilen. ", picture: { url: 'https://www.rnz.de/cms_media/module_img/580/290036_2_org_image_62e5f9b6249e3cf6.jpg' } });
    this.organisations.push( { title: "Universität Konstanz", description: "Hier können alle Studierende der Uni Konstanz ihre Werkzeuge teilen.", picture: { url: 'https://www.stuve.uni-konstanz.de/typo3temp/secure_downloads/107687/0/bb363740b16188c3c59371ad2dcc14fa14172ba8/csm_StuVe_Logo_188552d9da.png' } });
    this.organisations.push( { title: "Bürgerbüro Konstanz", description: "Auch das Bürgerbüro stellt verschiedene Dinge für Jedermann zur freien Verfügung.", picture: { url: 'https://www.konstanz.de/site/Konstanz/get/params_E486985068/77390/B%C3%BCrgerb%C3%BCro%20VGL_1640_690.jpg' } });
    this.organisations.push( { title: "Gartenfreunde Konstanz", description: "Garten-Tools des Vereins der Gartenfreude am Bismarckturm e.V. ", picture: { url: 'https://www.gartenfreunde-konstanz.de/blume10_448_336.jpg' } });
    this.organisations.push( { title: "Bürgergemeinschaft Petershausen e.V.", description: "Hole Tools ab im Herzen Petershausens", picture: { url:'https://www.bg-petershausen.de/medien/Bilder/2010-04-17_Rundgang_Petershausen_Ost_1/Petershauser%20Park.jpg'} });
  
  }


  ngOnInit(): void {
    this.addDefaultOrganisations();
  }
*/

    /* Tutorial von https://levelup.gitconnected.com/simple-application-with-angular-6-node-js-express-2873304fff0f
  registered = false;
	submitted = false;
	userForm: FormGroup;

  constructor(private formBuilder: FormBuilder)
  {

  }

  invalidFirstName()
  {
  	return (this.submitted && this.userForm.controls.first_name.errors != null);
  }

  invalidLastName()
  {
  	return (this.submitted && this.userForm.controls.last_name.errors != null);
  }

  invalidEmail()
  {
  	return (this.submitted && this.userForm.controls.email.errors != null);
  }

  invalidZipcode()
  {
  	return (this.submitted && this.userForm.controls.zipcode.errors != null);
  }

  invalidPassword()
  {
  	return (this.submitted && this.userForm.controls.password.errors != null);
  }


  ngOnInit(){
    this.userForm = this.formBuilder.group({
  		first_name: ['', Validators.required],
  		last_name: ['', Validators.required],
  		email: ['', [Validators.required, Validators.email]],
  		zipcode: ['', [Validators.required, Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')]],
  		password: ['', [Validators.required, Validators.minLength(5), Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')]],
  	});
  }

  onSubmit()
  {
  	this.submitted = true;

  	if(this.userForm.invalid == true)
  	{
  		return;
  	}
  	else
  	{
  		this.registered = true;
  	}
  }*/

}
