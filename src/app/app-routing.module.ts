import { RegisterComponent } from './register/register.component';
import { ToolCreateComponent } from './tool-create/tool-create.component';
import { OrganisationComponent } from './organisation/organisation.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { CreateOrganisationComponent } from './create-organisation/create-organisation.component';
import { FaqComponent } from './faq/faq.component';
import { FrontendDevelopmentComponent } from './frontend-development/frontend-development.component';
import { ContactComponent } from './contact/contact.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { OrganizationPageComponent } from './organization-page/organization-page.component';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'organisation', component: OrganisationComponent },
  { path: 'create-tool', component: ToolCreateComponent },
  { path: 'login', component: LoginComponent},
  { path: 'registration', component: RegisterComponent },
  { path: 'create-organisation', component: CreateOrganisationComponent},
  { path: 'faq', component: FaqComponent},
  { path: 'frontend-development', component: FrontendDevelopmentComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'about', component: ImpressumComponent},
  { path: 'organization-page/:orgaID', component: OrganizationPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
