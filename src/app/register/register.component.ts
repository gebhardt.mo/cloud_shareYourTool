
import { Component, OnInit, Input} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HomeComponent } from '../home/home.component';
import { OrganizationService } from './../services/organization.service';
import { organization } from '../models/organization';
import { Router } from '@angular/router';
import { UserService } from './../services/user.service';
import { user } from '../models/user';
import { SocialAuthService } from 'angularx-social-login';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  constructor(private router: Router, private userService: UserService, private authService: SocialAuthService, private snackbar: MatSnackBar) { }

    userToken = history.state.data.token
    
    email: string;
    firstName: string;
    lastName: string;
    phone: string;
    street: string;
    postCode: string;
    

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.minLength(3)]),
    firstName: new FormControl('', [Validators.required, Validators.minLength(3)]),
    lastName: new FormControl('', [Validators.required, Validators.minLength(3)]),
    phone: new FormControl('', [Validators.required, Validators.minLength(3)]),
    street: new FormControl('', [Validators.required, Validators.minLength(3)]),
    postCode: new FormControl('', [Validators.required, Validators.minLength(3)])
    

  });

 

get f(){
    return this.form.controls;
  }
 
submit(){
  // console.log("Submit method called");

    if(this.form.status === 'VALID'){

      const user: user = {
          id: '',
          email: this.form.value.email,
          firstName: this.form.value.firstName,
          lastName: this.form.value.lastName,
          street: this.form.value.street,
          postCode: this.form.value.postCode,
          tokenId: this.userToken,
          phone: this.form.value.phone,
          organizations: null
      }
      // console.log(user.firstName)
      // console.log(user.tokenId)
      this.userService.insertUser(user);

      this.authService.signOut();      
      this.router.navigateByUrl('/login');
      this.snackbar.open("USer wurde erfolgreich erstellt.", "X", {
        duration: 10000,
        horizontalPosition: "end",
        verticalPosition: "top",
      });
    } 
  }

ngOnInit(): void {}
  
}
