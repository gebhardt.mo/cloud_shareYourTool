import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OrganizationService } from './../services/organization.service';
import { organization } from '../models/organization';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-create-organisation',
  templateUrl: './create-organisation.component.html',
  styleUrls: ['./create-organisation.component.css']
})
export class CreateOrganisationComponent implements OnInit {

  constructor(
    private snackbar: MatSnackBar,
    private organizationService: OrganizationService,
    private router: Router
  ) { }

  userId: string;

  orgaName: string;
  orgaDescription: string;
  orgaDescriptionLong: string;
  orgaPicture = 'https://static4.suedkurier.de/storage/image/0/7/9/3/12873970_shift-966x593_1uxShP_LpX5ie.jpg';
  form: FormGroup;


  ngOnInit(): void {
    
    this.form = new FormGroup({
      orgaName: new FormControl('', [Validators.required, Validators.minLength(3)]),
      orgaDescription: new FormControl('', [Validators.required, Validators.minLength(3)]),
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    if (this.form.status === 'VALID') {

      const organization: organization = {
        nameOrg: this.form.value.orgaName,
        id: '',
        members: '',
        subtitle: this.form.value.orgaDescription,
        image: this.orgaPicture
      };

      this.organizationService.insertOrganization(organization).subscribe(() => {
        this.router.navigateByUrl('/home');
        this.snackbar.open("Organisation wurde erfolgreich erstellt.", "X", {
          duration: 10000,
          horizontalPosition: "end",
          verticalPosition: "top",
        });
      });


    }
  }






}
