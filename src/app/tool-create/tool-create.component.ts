import { tool } from './../models/tool';
import { ToolService } from './../services/tool.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { timer } from 'rxjs';

@Component({
  selector: 'app-tool-create',
  templateUrl: './tool-create.component.html',
  styleUrls: ['./tool-create.component.css']
})
export class ToolCreateComponent implements OnInit {

  constructor(private toolService: ToolService, private router: Router, private snackbar: MatSnackBar) { }

  toolDescription: string;
  toolDescriptionLong: string;
  toolPicture = 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/tools-everyone-should-own-1576682734.jpg?crop=0.502xw:1.00xh;0.498xw,0&resize=640:*';
  form: FormGroup;

  ngOnInit(): void {

    this.form = new FormGroup({
      toolDescription: new FormControl('', [Validators.required, Validators.minLength(3)]),
      toolDescriptionLong: new FormControl('', [Validators.required, Validators.minLength(10)])
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    if (this.form.status === 'VALID') {

      const tool: tool = {
        id: '',
        user_id: localStorage.getItem('userID'),
        image: this.toolPicture,
        designation: this.form.value.toolDescription,
        description: this.form.value.toolDescriptionLong,
        create: ''
      }

      this.toolService.insertTool(tool).subscribe(() => {
        const source = timer(1000);
        source.subscribe(() => {
          this.router.navigateByUrl('/home');
          this.snackbar.open("Tool wurde erfolgreich erstellt.", "X", {
            duration: 10000,
            horizontalPosition: "end",
            verticalPosition: "top",
          });
        });
      });





    }
  }




}
