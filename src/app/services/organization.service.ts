import { joinOrganization } from './../models/joinOrganization';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { organization } from '../models/organization';



@Injectable({
  providedIn: 'root'
})

export class OrganizationService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Access-Control-Allow-Origin' : '*'
    })
  };

  constructor(private http: HttpClient) {}

  getAllOrganizations(): Observable<organization[]> {
    return this.http.get<organization[]>('/api/organizations/READAllOrganization')
  }

  getOrganization(id: string) {
    return this.http.get('/api/organizations/READgetOrganization/' + id)
  }
  
  getAllOrganizationJoined(userID: string): Observable<organization[]> {
    return this.http.get<organization[]>('/api/users/READgetAllOrganizationJoined/' + userID)
  }

  getAllOrganizationNotJoined(userID: string): Observable<organization[]> {
    return this.http.get<organization[]>('/api/users/READgetAllOrganizationNotJoined/' + userID)
  }

  joinOrganization(joinOrganization: joinOrganization){
    let json = JSON.stringify(joinOrganization);
    // console.log(json);
    return this.http.post('/api/users/UPDATEuserOrga/', json, this.httpOptions);
  }

  insertOrganization(organization: organization): Observable<any>{
    let json = JSON.stringify(organization);
    // console.log(json);
    return this.http.post('/api/organizations/WRITEorganization/', json, this.httpOptions);
  }

  updateOrganization(organization: organization): Observable<void> {
    return this.http.put<void>('/api/organizations/api/organization/' + organization.id,
      organization
    )
  }

  deleteOrganization(id: string) {
    return this.http.delete('/api/organizations/api/organization/' + id)
  }
}
