import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { user } from '../models/user';


@Injectable({
  providedIn: 'root'
})

export class UserService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Access-Control-Allow-Origin' : '*'
    })
  };

  constructor(private http: HttpClient) {}

  //TODO: URL + Parameter anpassen application/json

  //TODO: creat getAllUserfromUser

  getAllUsers(): Observable<user[]> {
    return this.http.get<user[]>('/api/users/api/user')
  }

  getUser(id: string): Observable<user> {
    return this.http.get<user>('/api/users/READuser/' + id)
  }

  getUserByToken(token: string): Observable<user> {
    return this.http.get<user>('/api/users/READuserToken/' + token)
  }

  insertUser(user: user){
    var userJSON = JSON.stringify(user)
    // console.log(userJSON)
    return this.http.post<user>('/api/users/WRITEuser/', userJSON, this.httpOptions).subscribe((val) => {
      // console.log("POST call successful value returned in body", val);
    },
    response => {
      // console.log("POST call in error", response);
    },
      () => {
      // console.log("The POST observable is now completed.");
    });
  }

  updateUser(user: user): Observable<void> {
    return this.http.put<void>(
      '/api/users/api/user/' + user,
      user
    )
  }

  deleteUser(id: string) {
    return this.http.delete('http://backend-users:8082/api/users/api/user/' + id)
  }
}
