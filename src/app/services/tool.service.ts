import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tool } from '../models/tool';

@Injectable({
  providedIn: 'root'
})
export class ToolService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Access-Control-Allow-Origin' : '*'
    })
  };
  constructor(private http: HttpClient) {}

  getAllToolsFromUser(userID: string) {
    return this.http.get('/api/users/READgetAllToolsFromOneUser/' + userID)
  }

  getTool(toolID: string) {
    return this.http.get('/api/tools/READtool/' + toolID)
  }

  getAllToolsOfOrganization(orgaID: string): Observable<tool[]> {
    return this.http.get<tool[]>('/api/organizations/READgetToolsOrganization/' + orgaID)
  }

  insertTool(tool: tool): Observable<any>{
    let json = JSON.stringify(tool);
    // console.log(json);
    return this.http.post('/api/tools/WRITEtool/', json, this.httpOptions);
  }

  updateTool(tool: tool): Observable<void> {
    return this.http.put<void>(
      '/api/tools/api/tool/' + tool.id,
      tool
    )
  }

  deleteTool(id: string) {
    return this.http.delete('http://backend-tools:8081/api/tools/api/tool/' + id)
  }
}