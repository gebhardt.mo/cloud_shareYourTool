FROM node:12.16.3
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@9.1.5
COPY . /app
CMD ng serve --host 0.0.0.0 --disableHostCheck true
EXPOSE 4200